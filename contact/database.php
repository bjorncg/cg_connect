<?php
class CustomDatabase {
	private $hostname = "localhost";
	private $dbName;
	private $username;
	private $password;
	private $dbConnection;
	
	function __construct($hostname, $dbname, $username, $password) {
		$this->setUsername($username);
		$this->setPassword($password);
		$this->setDbName($dbname);
		$this->setHostname($hostname);
		
		$this->dbConnection = new mysqli(
			$this->getHostname(),
			$this->getUsername(),
			$this->getPassword(),
			$this->getDbName()
		) or die( "Could not connect to database" );
	}
	
	function __destruct(){
		if( isset( $this->getDbConnection() ) ){
			$this->dbConnection = null;
		}
	}
	
	public function getDbConnection() {		
		return $this->dbConnection;
	}
	
	private function getUsername() {
		return $this->username;
	}
	
	private function getPassword() {
		return $this->password;
	}
	
	private function getDbName() {
		return $this->dbName;
	}
	
	private function getHostname() {
		return $this->hostname;
	}
	
	private function setUsername($username) {
		$this->username = $username;
	}
	
	private function setPassword($password) {
		$this->password = $password;
	}
	
	private function setDbName($dbName) {
		$this->dbName = $dbName;
	}
	
	private function setHostname($hostname) {
		$this->hostname = $hostname;
	}
}

?>