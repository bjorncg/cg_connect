<?php

require "Services/Twilio.php";
require_once __DIR__ . '/../../includes/database.php';

$AccountSid = "AC1ac27252d7ffdaefbe973bf51bb8b8eb";
$AuthToken = "cfc69ecc6e528fbe1bd3c62c23b24f7b";
$twilioClient = new Services_Twilio($AccountSid, $AuthToken);
//$message = $_POST['sms_message'];
$message = "This is a test message from twilio";


function send_sms_members(){
	global $message;
	global $twilioClient;

	$members = getPhplistMembers();	
	
	echo "<pre>"; print_r($members); echo "</pre>";
	foreach ($members as $number => $name) {
		$sms = $twilioClient->account->sms_messages->create(
				// Step 6: Change the 'From' number below to be a valid Twilio number
				// that you've purchased, or the (deprecated) Sandbox number
				"347-789-7509",
	
				// the number we are sending to - Any phone number
				$number,
				// the sms body
				"Hey $name, $message"
		);
	
		// Display a confirmation message on the screen
		echo "Sent message to $number";
		//var_dump($name);
	}
	
	closeDbConnection($mysqli);
}

function send_sms_subscribers(){
	global $message;
	global $twilioClient;
	
	$subscribers = getPhplistSubscribers();
	echo "<pre>"; print_r($subscribers); echo "</pre>";
	foreach ($subscribers as $number => $name) {
		$sms = $twilioClient->account->sms_messages->create(
				// Step 6: Change the 'From' number below to be a valid Twilio number
				// that you've purchased, or the (deprecated) Sandbox number
				"347-789-7509",

				// the number we are sending to - Any phone number
				$number,
				// the sms body
				"Hey $name, Welcome to Local 420!, As a reminder board meeting is schedule for thursday sep/12/2013 @ 6pm"
		);

		// Display a confirmation message on the screen
		echo "Sent message to $number";
		//var_dump($name);
	}
}

send_sms_members();
send_sms_subscribers();
?>