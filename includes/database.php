<?php

function getPhpListDbConnection($debug = false){
	if($debug){
		echo "Entering: " . __FUNCTION__ . "()\n";
	}
	
	$host="mysql51-062.wc1.ord1.stabletransit.com"; // Host name
	$username="712101_phplist"; // Mysql username
	$password="157Amadison"; // Mysql password
	$db_name="712101_phplistdb"; // Database name
	$mysqli = new mysqli($host, $username, $password, $db_name);
	
	if($debug){
		echo "Leaving: " . __FUNCTION__ . "()\n";
	}
	
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		die();
	}
	
	return $mysqli;
}

function getLocal420DbConnection($debug = false){
	echo "Entering: " . __FUNCTION__ . "()\n";
	
	$host="mysql51-073.wc1.ord1.stabletransit.com"; // Host name
	$username="712101_local420"; // Mysql username
	$password="L0cal420_cloud"; // Mysql password
	$db_name="712101_local420db"; // Database name
	$mysqli = new mysqli($host, $username, $password, $db_name);
	
	if($debug){
		echo "Leaving: " . __FUNCTION__ . "()\n";
	}
	
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		die();
	}
	return $mysqli;
}

function closeDbConnection($dbHandle, $debug = false){
	if($debug){
		echo "Entering: " . __FUNCTION__ . "()\n";
	}
	if ( $dbHandle ){
		$dbHandle->close();
	}
	echo "Leaving: " . __FUNCTION__ . "()\n";
}

function getPhplistMembers($debug = false){
	if($debug){
		echo "Entering: " . __FUNCTION__ . "()\n";
	}

	$mysqli = getPhpListDbConnection();
	
	$sql = "SELECT u.id,u.fname,u.lname,u.middle_initial,u.email,u.phone_number\n"
			. "FROM phplist_user_user u, phplist_list pl, phplist_listuser lu\n"
			. "WHERE u.id = lu.userid AND lu.listid=pl.id and pl.name='local420_members'";
	
	$result = $mysqli->query($sql);
	$members = array();
	
	while($row = $result->fetch_assoc()){
		if( $row["phone_number"] != ""){
			$number = $row["phone_number"];
			$name = $row["fname"]. " " . $row["lname"];
			//array_push($members, array($number, $name));
			$members[$number] = $name;
		}
	}
	closeDbConnection($mysqli);
	
	if($debug){
		echo "Leaving: " . __FUNCTION__ . "()\n";
	}
	
	return $members;
}

function getPhplistSubscribers($debug = false){
	if($debug){
		echo "Entering: " . __FUNCTION__ . "()\n";
	}
	
	$mysqli = getPhpListDbConnection();

	$sql = "SELECT u.id,u.fname,u.lname,u.middle_initial,u.email,u.phone_number\n"
			. "FROM phplist_user_user u, phplist_list pl, phplist_listuser lu\n"
			. "WHERE u.id = lu.userid AND lu.listid=pl.id and pl.name='local420_subscribers'";

	$result = $mysqli->query($sql);
	$subscribers = array();

	while($row = $result->fetch_assoc()){
		if( $row["phone_number"] != ""){
			$number = $row["phone_number"];
			$name = $row["fname"]. " " . $row["lname"];
			//array_push($subscribers, array($number, $name));
			$subscribers[$number] = $name;
		}
	}
	closeDbConnection($mysqli);
	
	if($debug){
		echo "Leaving: " . __FUNCTION__ . "()\n";
	}
	
	return $subscribers;
}


function getLocal420Subscribers($debug = false){
	if($debug){
		echo "Entering: " . __FUNCTION__ . "()\n";
	}
	
	$mysqli = getLocal420DbConnection();

	$sql = "SELECT u.cell_phone, u.cell_name FROM CellPhone";

	$result = $mysqli->query($sql);
	$members = array();

	while($row = $result->fetch_assoc()){
		
			$number = $row["cell_phone"];
			$name = $row["cell_name"];
			//array_push($subscribers, array($number, $name));
			$members[$number] = $name;
		
	}
	closeDbConnection($mysqli);
	
	if($debug){
		echo "Leaving: " . __FUNCTION__ . "()\n";
	}
	
	return $members;
}


?>